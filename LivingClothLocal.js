ServerSocket.on("ChatRoomMessage", function (data) { ChatRoomMessageAdd(data); });

if (typeof ChatRoomMessageAdditionDict === 'undefined') {
  ChatRoomMessageAdditionDict = {}
}

function ChatRoomMessageAdd(data) {

	// Make sure the message is valid (needs a Sender and Content)
	if ((data != null) && (typeof data === "object") && (data.Content != null) && (typeof data.Content === "string") && (data.Content != "") && (data.Sender != null) && (typeof data.Sender === "number")) {

		// Make sure the sender is in the room
		var SenderCharacter = null;
		for (var C = 0; C < ChatRoomCharacter.length; C++)
			if (ChatRoomCharacter[C].MemberNumber == data.Sender) {
				SenderCharacter = ChatRoomCharacter[C]
				break;
			}

		// If we found the sender
		if (SenderCharacter != null) {

			// Replace < and > characters to prevent HTML injections
			var msg = data.Content;
			while (msg.indexOf("<") > -1) msg = msg.replace("<", "&lt;");
			while (msg.indexOf(">") > -1) msg = msg.replace(">", "&gt;");


      // This part is to append code react to certain message
      for (var key in ChatRoomMessageAdditionDict) {
        ChatRoomMessageAdditionDict[key](SenderCharacter, msg, data)
      }
    }
  }
}

function AcquireplayerName(player) {
    var playerName = player.Nickname
    if (playerName === "" || playerName === undefined) {
        playerName = player.Name
    }
    return playerName
}

function GenerateRandomNumber(n) {
// generate random number between 0 and n
    return Math.floor(Math.random() * n)
}

PlayerState = null
ExpPerLevel = [0, 100, 250, 450, 700, 1000]

class LivingClothStateStorage {
    // constructor(playerId, playerName, level, satiety, excitement, exp) {
    constructor(state) {
        this.playerId = state.playerId
        this.playerName = state.playerName
        this.level = state.level
        this.satiety = state.satiety
        this.excitement = state.excitement
        this.exp = state.exp
    }
}

class LivingClothState {

    constructor(obj) {
        if (typeof(obj) == "object") {
            this.constructFromStorage(obj)
        } else {
            this.constructInit(obj)
        }
    }

    constructFromStorage(storage) {
        this.playerId = storage.playerId
        this.playerName = storage.playerName
        this.level = storage.level
        this.satiety = storage.satiety
        this.excitement = storage.excitement
        this.exp = storage.exp
    }

    constructInit(playerId) {
        this.playerId = playerId
        this.playerName= AcquireplayerName(Player)
        this.level = 0
        this.satiety = 50
        this.excitement = 0
        this.exp = 0
    }

    levelUp(player) {
        this.level++
        // console.log("level: " + this.level)
        switch (this.level) {
            case 1:
                WearLevel1(player)
                break
            case 2:
                WearLevel2(player)
                break
            case 3:
                WearLevel3(player)
                break
            case 4:
                WearLevel4(player)
                break
            case 5:
                WearLevel5(player)
                break
        }
        this.updateLivingClothName(player, this.level)
        var content = "*寄生在" + this.playerName + "身上的触手服升级了."
        SendChat("Emote", content)
    }

    expUp(player, n) {
        this.exp += GenerateRandomNumber(n)
        // console.log("exp: " + this.exp)
        if (this.level < ExpPerLevel.length - 1 && this.exp >= ExpPerLevel[this.level+1]) {
            this.levelUp(player)
        }
    }

    satietyUp(n) {
        this.satiety = Math.min(this.satiety + GenerateRandomNumber(n), 100)
    }

    satietyDecrease(n) {
        this.satiety = Math.max(this.satiety - GenerateRandomNumber(n), 0)
    }

    excitementUp(n) {
        this.excitement = Math.min(this.excitement + GenerateRandomNumber(n), 100)
    }

    excitementDecrease(n) {
        this.excitement = Math.max(this.excitement - GenerateRandomNumber(n), 0)
    }

    updateLivingClothName(player, level) {
        var bodyPart = ["ItemPelvis", "ItemTorso", "ItemBreast", "ItemLegs", "ItemFeet", "ItemBoots", "ItemArms"]
        var n = level
        if (n > 2) {
            n++
        }
        for (var i = 0; i < n; i++) {
            var livingCloth = InventoryGet(player, bodyPart[i])
            var name = livingCloth.Craft.Name
            name = name.substring(0, name.length - 1) + String(level)
            // console.log("update_name: " + name)
            livingCloth.Craft.Name = name
        }
        ChatRoomCharacterUpdate(player)
    }
}

function AddArousalProgress(player, n) {
    var up = GenerateRandomNumber(n)
    Player.ArousalSettings.Progress += up
    ActivityChatRoomArousalSync(Player)
    if (Player.ArousalSettings.Progress >= 100) {
        ActivityOrgasmStart(Player)
    }
}

function SendChat(type, content) {
    ServerSend("ChatRoomChat", { Content: content, Type: type} )
}

function actionLick(level, player, playerName) {
    var bodyPart = ["阴部", "豆豆", "屁股"]
    var actions =  ["轻舔", "抚弄"]
    if (level >= 1) {
        bodyPart.push("肚肚");
        bodyPart.push("后背");
    }
    if (level >= 2) {
        bodyPart.push("馒头");
        bodyPart.push("葡萄");
    }
    if (level >= 3) {
        bodyPart.push("小腿");
        bodyPart.push("大腿");
    }
    if (level >= 4) {
        bodyPart.push("脚趾");
        bodyPart.push("脚心");
        bodyPart.push("脚脚");
    }
    if (level >= 5) {
        bodyPart.push("腋下");
        bodyPart.push("脖子");
        bodyPart.push("爪爪");
    }
    var bpn = GenerateRandomNumber(bodyPart.length)
    var acn = GenerateRandomNumber(actions.length)
    var content = "*触手" + actions[acn] + playerName + "的" + bodyPart[bpn] + "."
    SendChat("Emote", content)
    AddArousalProgress(player, 20)
}

function actionTwine(level, player, playerName) {
    var bodyPart = ["豆豆"]
    var actions = ["缠绕", "紧紧缠绕"]
    if (level >= 1) {
        bodyPart.push("腰");
    }
    if (level >= 2) {
        bodyPart.push("馒头根部");
        bodyPart.push("葡萄");
    }
    if (level >= 3) {
        bodyPart.push("小腿");
        bodyPart.push("大腿");
    }
    if (level >= 4) {
        bodyPart.push("脚趾");
        bodyPart.push("脚心");
        bodyPart.push("脚脚");
    }
    if (level >= 5) {
        bodyPart.push("爪爪");
    }
    var bpn = GenerateRandomNumber(bodyPart.length)
    var acn = GenerateRandomNumber(actions.length)
    var content = "*触手" + actions[acn] + "在" + playerName + "的" + bodyPart[bpn] + "上."
    SendChat("Emote", content)
    AddArousalProgress(player, 30)
}

function actionSuck(level, player, playerName) {
    var bodyPart = ["豆豆"]
    var actions = ["表面的吸盘吸在了"]
    if (level >= 2) {
        bodyPart.push("葡萄");
    }
    if (level >= 4) {
        bodyPart.push("脚心");
    }
    if (level >= 5) {
        actions.push("表面的吸盘长出细小的毛刺，狠狠得吸在了");
    }
    var bpn = GenerateRandomNumber(bodyPart.length)
    var acn = GenerateRandomNumber(actions.length)
    var content = "*触手" + actions[acn] + playerName + "的" + bodyPart[bpn] + "上."
    SendChat("Emote", content)
    if (acn == 0) {
        AddArousalProgress(player, 40)
    } else {
        AddArousalProgress(player, 60)
    }
}

BCToyDict = {"有线跳蛋": "WiredEgg",
             "跳蛋": "TapedVibeEggs",
             "肛塞": "EggVibePlugXXL",
             "肛钩": "AnalHook"}
BCBodyDict = {"小穴": "ItemVulva",
              "葡萄": "ItemNipples",
              "屁股": "ItemButt"}
BCToyMode = ["Off", "Low", "Medium", "High", "Maximum"]

function WearToy(player, bp, toy) {
    var craft = {}
    var property = {}
    InventoryWear(player, toy, bp, 'Default', '999', Craft=craft)
    var description = "由触手拟态而成的玩具，本质上仍然是触手，只是多了一层掩人耳目的伪装."
    var BCToy = InventoryGet(player, bp)
    if (toy == "WiredEgg") {
        craft = {"Item":"WiredEgg","Property":"Arousing","Lock":"","Name":"拟态跳蛋","Description":description,"Color":"Default,Default","Private":true,"Type":"Low","ItemProperty":{},"MemberNumber":-1,"MemberName":"母巢"}
        property = {"Effect":["Egged","Vibrating"],"Mode":"Low","Intensity":0}
    } else if (toy == "TapedVibeEggs") {
        craft = {"Item":"TapedVibeEggs","Property":"Arousing","Lock":"","Name":"拟态跳蛋","Description":description,"Color":"Default,Default,Default,Default","Private":true,"Type":"Low","ItemProperty":{},"MemberNumber":-1,"MemberName":"母巢"}
        property = {"Effect":["Egged","Vibrating"],"Mode":"Low","Intensity":0}
    } else if (toy == "EggVibePlugXXL") {
        craft = {"Item":"EggVibePlugXXL","Property":"Arousing","Lock":"","Name":"拟态肛塞","Description":description,"Color":"Default","Private":true,"Type":"Low","ItemProperty":{},"MemberNumber":-1,"MemberName":"母巢"}
        property = {"Effect":["Egged","Vibrating"],"Mode":"Low","Intensity":0}
    } else {
        craft = {"Item":"AnalHook","Property":"Arousing","Lock":"","Name":"拟态肛钩","Description":description,"Color":"Default","Private":true,"Type":"Hair","ItemProperty":{},"MemberNumber":-1,"MemberName":"母巢"}
        property = {"Difficulty":4,"Intensity":1,"Effect":["Egged"],"Type":"Hair"}
    }
    BCToy.Property = property
    BCToy.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function StrengThenToy(player, bp) {
    var BCToy = InventoryGet(player, bp)
    if (BCToy.Property.Mode) {
        var pos = BCToyMode.indexOf(BCToy.Property.Mode)
        // ("pos: " + pos)
        if (pos >=0 && pos < BCToyMode.length - 1) {
            pos++
            BCToy.Property.Mode = BCToyMode[pos]
            // console.log("Mode: " + BCToy.Property.Mode)
        } else {
            console.error("Wrong toy mode: " + BCToy.Property.Mode + ", toy: " + BCToy.Name)
        }
    } else {
        console.error("Wrong toy mode: " + BCToy.Property.Mode + ", toy: " + BCToy.Name)
    }
    if (BCToy.Property.Intensity != null && BCToy.Property.Intensity >= -1 && BCToy.Property.Intensity < 3) {
        BCToy.Property.Intensity += 1
        // console.log("Intensity: " + BCToy.Property.Intensity)
    } else {
        console.warn("Wrong toy Intensity: " + BCToy.Property.Intensity + ", toy: " + BCToy.Name)
    }
    ChatRoomCharacterUpdate(player)
}

function actionToy(level, player, playerName) {
    var bodyPart = ["小穴"]
    var toys = ["有线跳蛋"]
    if (level >= 2) {
        bodyPart.push("葡萄");
        toys.push("跳蛋")
    }
    if (level >= 4) {
        bodyPart.push("屁股")
        toys.push("肛塞")
    }
    if (level >= 5) {
        bodyPart.push("屁股")
        toys.push("肛钩")
    }
    var n = GenerateRandomNumber(bodyPart.length)
    var needWear = true
    var playerId = player.MemberNumber
    player = Player
    if (InventoryIsWorn(player, BCToyDict[toys[n]], BCBodyDict[bodyPart[n]])) {
        var BCToy = InventoryGet(player, BCBodyDict[bodyPart[n]])
        if (BCToy.Craft && BCToy.Craft.Name.indexOf("拟态") != -1) {
            needWear = false
        }
    }
    if (needWear) {
        var content = "*触手拟态出了一个" + toys[n] + "，装在了" + playerName + "的" + bodyPart[n] + "上."
        SendChat("Emote", content)
        WearToy(player, BCBodyDict[bodyPart[n]], BCToyDict[toys[n]])
    } else {
        var content = "*触手拟态成的" + toys[n] + "动的更厉害了."
        SendChat("Emote", content)
        StrengThenToy(player, BCBodyDict[bodyPart[n]])
    }
}

function AddLSCGHornyLevel(player, n) {
    var up = GenerateRandomNumber(n)
    Player.LSCG.InjectorModule.hornyLevel = Math.min(Player.LSCG.InjectorModule.hornyLevel + up , 400)
    ChatRoomCharacterUpdate(Player)
}

function actionSpray(level, player, playerName) {
    var bodyPart = ["阴部", "豆豆", "屁股"]
    var actions = ["涂抹"]
    if (level >= 1) {
        bodyPart.push("肚肚");
        bodyPart.push("后背");
    }
    if (level >= 2) {
        bodyPart.push("馒头");
        bodyPart.push("葡萄");
    }
    if (level >= 3) {
        bodyPart.push("小腿");
        bodyPart.push("大腿");
    }
    if (level >= 4) {
        bodyPart.push("脚趾");
        bodyPart.push("脚心");
        bodyPart.push("脚脚");
    }
    if (level >= 5) {
        bodyPart.push("腋下");
        bodyPart.push("脖子");
        bodyPart.push("爪爪");
    }
    AddLSCGHornyLevel(player, (level - 3) * 100)
    var bpn = GenerateRandomNumber(bodyPart.length)
    var acn = GenerateRandomNumber(actions.length)
    var content = "*触手分泌出媚药，" + actions[acn] + "在" + playerName + "的" + bodyPart[bpn] + "上."
    SendChat("Emote", content)
}

function LivingClothAction(level, player, playerName) {
    var n = GenerateRandomNumber(100)
    switch(level) {
        case 0:
            if (n < 80) {
                actionLick(level, player, playerName)
            } else {
                actionSuck(level, player, playerName)
            }
            break
        case 1:
            if (n < 30) {
                actionLick(level, player, playerName)
            } else if (30 <= n && n < 70) {
                actionTwine(level, player, playerName)
            } else {
                actionSuck(level, player, playerName)
            }
            break
        case 2:
            if (n < 20) {
                actionLick(level, player, playerName)
            } else if (20 <= n && n < 60) {
                actionTwine(level, player, playerName)
            } else {
                actionSuck(level, player, playerName)
            }
            break
        case 3:
            if (n < 10) {
                actionLick(level, player, playerName)
            } else if (10 <= n && n < 40) {
                actionTwine(level, player, playerName)
            } else if (40 <= n && n < 60) {
                actionSuck(level, player, playerName)
            } else {
                actionToy(level, player, playerName)
            }
            break
        case 4:
            if (n < 10) {
                actionLick(level, player, playerName)
            } else if (10 <= n && n < 25) {
                actionTwine(level, player, playerName)
            } else if (25 <= n && n < 50) {
                actionSuck(level, player, playerName)
            } else if (50 <= n && n < 75){
                actionToy(level, player, playerName)
            } else {
                actionSpray(level, player, playerName)
            }
            break
        case 5:
            if (n < 10) {
                actionLick(level, player, playerName)
            } else if (10 <= n && n < 25) {
                actionTwine(level, player, playerName)
            } else if (25 <= n && n < 50) {
                actionSuck(level, player, playerName)
            } else if (50 <= n && n < 75){
                actionToy(level, player, playerName)
            } else {
                actionSpray(level, player, playerName)
            }
            break
        default:
            console.error("wrong level: " + level)
            break
    }
}

function WearLevel0(player, level = 0) {
    var craft = {
        Item: "SciFiPleasurePanties",
        Property: "Arousing",
        Name: "触手内裤Lv." + level,
        Description: "触手拟态而成的胖次，是诅咒的初始形态。遵循着触手的本能，会抚摸、舔舐周遭的事物。",
        Color: "#aa80aa,#aa80aa,#aa80aa,#aa80aa,#aa80aa,#aa80aa,Default",
        Private: true,
        Type: "c3i0o0s0",
        ItemProperty: {"ShowText": true},
        MemberNumber: -1,
        MemberName: "母巢"
    }
    InventoryWear(player, "SciFiPleasurePanties", "ItemPelvis", 'Default', '20', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemPelvis")
    livingCloth.Color = ["#aa80aa","#aa80aa","#aa80aa","#aa80aa","#aa80aa","#aa80aa","Default"]
    livingCloth.Property.Block = ["ItemVulva","ItemVulvaPiercings","ItemButt"]
    livingCloth.Property.Effect = ["UseRemote","Egged","UseRemote","Chaste","ButtChaste"]
    livingCloth.Property.Hide = ["Pussy"]
    livingCloth.Property.HideItem = ["ItemButtAnalBeads2","ItemVulvaVibratingDildo","ItemVulvaClitSuctionCup","ItemVulvaInflatableVibeDildo","ItemVulvaHeavyWeightClamp","ItemVulvaPenisDildo","ItemVulvaShockDildo","ItemVulvaPiercingsVibeHeartClitPiercing","ItemVulvaPiercingsClitRing","ItemVulvaPiercingsChastityClitShield","ItemVulvaPiercingsHighSecurityVulvaShield","ItemVulvaPlasticChastityCage1","ItemVulvaPlasticChastityCage2","ItemVulvaTechnoChastityCage","ItemVulvaFlatChastityCage","ItemVulvaVibeEggPenisBase"]
    livingCloth.Property.AllowActivity = []
    livingCloth.Property.Attribute = ["GenitaliaCover","FuturisticRecolor"]
    livingCloth.Property.ShowText = true
    livingCloth.Property.Type = "c3i0o0s0"
    livingCloth.Property.Intensity = -1
    livingCloth.Property.ShockLevel = 0
    livingCloth.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function WearLevel1(player, level = 1) {
    var craft = {
        Item: "ClassicLatexCorset",
        Property: "Secure",
        Name: "触手束腰Lv." + level,
        Description: "触手拟态而成的束腰，吸食了足够的能量，诅咒得到了初步的发展。触手的力量和柔韧性得到了增强，会缠绕在它喜欢的事物上，用表现的吸盘不间断地吮吸。",
        Color: "Default,#aa80aa,#202020",
        Private: true,
        Type: null,
        ItemProperty: {},
        MemberNumber: -1,
        MemberName: "母巢"
    }
    InventoryWear(player, "ClassicLatexCorset", "ItemTorso", "Default,#aa80aa,#202020", '60', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemTorso")
    livingCloth.Color = ["Default","#aa80aa","#202020"]
    livingCloth.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function WearLevel2(player, level = 2) {
    var craft = {
        Item: "TickleBra",
        Property: "Arousing",
        Name: "触手胸罩Lv." + level,
        Description: "触手拟态而成的罩罩，不满足于现有的“领土”，诅咒的范围进一步扩大。",
        Color: "#aa80aa,#202020,#aa8080,#aa8080",
        Private: true,
        Type: "Off",
        ItemProperty: {},
        MemberNumber: -1,
        MemberName: "母巢"
    }
    InventoryWear(player, "TickleBra", "ItemBreast", "Default", '120', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemBreast")
    livingCloth.Color = ["#aa80aa","#202020","#aa8080","#aa8080"]
    livingCloth.Property.Mode = "Off"
    livingCloth.Property.Intensity = -1
    livingCloth.Property.Effect = ["Egged"]
    livingCloth.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function WearLevel3(player, level = 3) {
    var craft = {
        Item: "LeatherDeluxeLegCuffs",
        Property: "Secure",
        Name: "触手腿铐Lv." + level,
        Description: "触手拟态而成的腿铐，可以限制宿主的行动。诅咒得到了进一步的强化。",
        Color: "#808080,#aa80aa,Default,Default,Default",
        Private: true,
        Type: "Chained",
        ItemProperty: {},
        MemberNumber: -1,
        MemberName: "母巢"
    }
    InventoryWear(player, "LeatherDeluxeLegCuffs", "ItemLegs", "Default", '200', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemLegs")
    livingCloth.Color = ["#808080","#aa80aa","Default","Default","Default"]
    livingCloth.Property.Type = "Chained"
    livingCloth.Property.Effect = ["Slow"]
    livingCloth.Craft = craft

    var craft = {
        Item: "LeatherDeluxeAnkleCuffs",
        Property: "Secure",
        Name: "触手脚铐Lv." + level,
        Description: "触手拟态而成的脚铐，可以限制宿主的行动。诅咒得到了进一步的强化。",
        Color: "#808080,#aa80aa,Default,Default,Default",
        Private: true,
        Type: "Chained",
        ItemProperty: {},
        MemberNumber: -1,
        MemberName: "母巢"
    }
    InventoryWear(player, "LeatherDeluxeAnkleCuffs", "ItemFeet", "Default", '200', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemFeet")
    livingCloth.Color = ["#808080","#aa80aa","Default","Default","Default"]
    livingCloth.Property.SetPose = []
    livingCloth.Property.FreezeActivePose = []
    livingCloth.Property.Type = "Chained"
    livingCloth.Property.Effect = ["Slow"]
    livingCloth.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function WearLevel4(player, level = 4) {
    var craft = {
        Item: "BalletHeels",
        Property: "Secure",
        Name: "触手高跟鞋Lv." + level,
        Description: "触手拟态而成的高跟鞋，在限制宿主行动的同时，还会不停地舔舐宿主敏感的脚心。随着诅咒的增强，触手将更加活跃。",
        Color: "#aa80aa",
        Private: true,
        Type: null,
        ItemProperty: {},
        MemberNumber: -1,
        MemberName: "母巢"
    }
    InventoryWear(player, "BalletHeels", "ItemBoots", "Default", '500', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemBoots")
    livingCloth.Color = ["#aa80aa"]
    livingCloth.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function WearLevel5(player, level = 5) {
    var craft = {
        Item: "BitchSuit",
        Property: "Secure",
        Lock: "ExclusivePadlock",
        Name: "触手服Lv." + level,
        Description: "触手服的终极形态，亦是诅咒的终极形态。",
        Color: "#aa80aa,#aa80aa,#cc33cc,#808080,#bbbbbb",
        Private: true,
        Type: "z1st1cl0un0",
        ItemProperty: {},
        MemberNumber:  -1,
        MemberName: "母巢",
    }

    InventoryWear(player, "BitchSuit", "ItemArms", 'Default', '9999', Craft=craft)
    var livingCloth = InventoryGet(player, "ItemArms")
    livingCloth.Color = ["#aa80aa","#aa80aa","#cc33cc","#808080","#bbbbbb"]
    livingCloth.Property.Block = ["ItemPelvis","ItemTorso","ItemTorso2","ItemHands","ItemHandheld","ItemBreast","ItemNipples","ItemNipplesPiercings","ItemVulva","ItemVulvaPiercings","ItemButt"]
    livingCloth.Property.Effect = ["Block","Prone","ForceKneel","Slow","Lock"]
    livingCloth.Property.Hide = ["Shoes","ItemBoots","ItemLegs","ItemFeet","Gloves","Hands","LeftHand","RightHand","ItemHandheld","ItemHands","ItemNipples","ItemNipplesPiercings","ItemBreast","ItemVulva","ItemVulvaPiercings","Suit","SuitLower","Cloth","ClothLower","Corset","Garters","Socks","SocksLeft","SocksRight"]
    livingCloth.Property.HideItem = ["ItemNipplesLactationPump","ClothAccessoryPoncho","NecklaceBodyChainNecklace","SuitBlouse1","BraLatexBunnySuit","BraHeartTop","BraCamisole","BraStuddedHarness","PantiesBikini1","PantiesHarnessPanties2","SocksSocks6","SocksSocksFur"]
    livingCloth.Property.AllowActivity = []
    livingCloth.Property.Attribute = []
    livingCloth.Property.Type = "z1st1cl0un0"
    livingCloth.Craft = craft
    ChatRoomCharacterUpdate(player)
}

function GainExp(SenderCharacter, msg, data) {
    if (data.Type == "Activity" && msg.indexOf("Orgasm") != -1 && msg.indexOf("Resist") == -1) {
        var id = SenderCharacter.MemberNumber
        if (id == Player.MemberNumber) {
            var state = PlayerState
            if (state != null) {
                state.expUp(SenderCharacter, 5)
                state.satietyUp(50)
            }
        }
    }
}
ChatRoomMessageAdditionDict["LivingClothGainExp"] = function(SenderCharacter, msg, data) {GainExp(SenderCharacter, msg, data)}

function LivingClothProgress(player) {
    var playerName = AcquireplayerName(player)
    var id = player.MemberNumber
    var state = PlayerState
    if (state) {
        var level = state.level
        var satiety = state.satiety
        // var excitement = state.excitement
        var n = GenerateRandomNumber(100)
        if (n >= satiety) {
            LivingClothAction(level, player, playerName)
        }
    }
}

satietyLogFlag = true
function LivingClothSatiety(player) {
    var playerName = AcquireplayerName(player)
    var id = player.MemberNumber
    var state = PlayerState
    if (state) {
        var level = state.level
        var n = GenerateRandomNumber(5 + level)
        state.satiety  = Math.max(state.satiety - n, 0)
        if (satietyLogFlag) {
            if (state.satiety < 40) {
                if (state.satiety >= 20) {
                    var content = "*" + playerName + "身上的触手渴求着新鲜的体液."
                    SendChat("Emote", content)
                } else if (state.satiety < 20 && state.satiety >= 10) {
                    var content = "*" + playerName + "身上的触手难以压抑进食的欲望."
                    SendChat("Emote", content)
                } else {
                    var content = "*" + playerName + "身上的触手迫不及待地想要去品尝被它所捕获的涩喵."
                    SendChat("Emote", content)
                }
                satietyLogFlag = false;
                setTimeout(function() { satietyLogFlag = true; }, 300000)
            }
        }
    }
}

function LivingClothRestore(player) {
    var items = ["SciFiPleasurePanties", "ClassicLatexCorset", "TickleBra", "LeatherDeluxeLegCuffs",
                 "LeatherDeluxeAnkleCuffs", "BalletHeels", "BitchSuit"]
    var bodyPart = ["ItemPelvis", "ItemTorso", "ItemBreast", "ItemLegs", "ItemFeet", "ItemBoots", "ItemArms"]
    var itemName = ["触手内裤", "触手束腰", "触手胸罩", "触手腿铐", "触手脚铐", "触手高跟鞋", "触手服"]
    var flag = false
    var needRestore = false
    var restoreLogFlag = false
    var playerName = AcquireplayerName(player)
    var id = player.MemberNumber
    player = Player
    var state = PlayerState
    if (state) {
        var level = state.level
        for (var i = 0; i <= level + 1; ++i) {
            if (InventoryIsWorn(player, items[i], bodyPart[i])) {
                var LivingCloth = InventoryGet(player, bodyPart[i])
                if (LivingCloth.Craft && LivingCloth.Craft.Name.indexOf(itemName[i]) != -1) {
                    needRestore = false
                } else {
                    needRestore = true
                    restoreLogFlag = true
                }
            } else {
                needRestore = true
                restoreLogFlag = true
            }
            if (needRestore) {
                switch (i) {
                    case 0:
                        WearLevel0(player, level)
                        break
                    case 1:
                        WearLevel1(player, level)
                        break
                    case 2:
                        WearLevel2(player, level)
                        break
                    case 3:
                        WearLevel3(player, level)
                        break
                    case 4:
                        WearLevel3(player, level)
                        break
                    case 5:
                        WearLevel4(player, level)
                        break
                    case 6:
                        WearLevel5(player, level)
                        break
                }
            }
            if (level < 3 && i == level) {
                break
            }
            if (level >= 3 && i == 3) {
                i++
            }
        }
    }
    if (restoreLogFlag) {
        var content = "*诅咒在" + playerName + "身上重新苏醒."
        SendChat("Emote", content)
    }
}

function saveState() {
    var state = PlayerState
    if (state) {
        var storage_key = "BC_LivingClothState"
        var storage_value = new LivingClothStateStorage(state)
        localStorage.setItem(storage_key, JSON.stringify(storage_value))
    }
}

function init() {
    var storage_key = "BC_LivingClothState"
    var storage_state = JSON.parse(localStorage.getItem(storage_key))
    if (storage_state) {
        var state = new LivingClothState(storage_state)
        PlayerState = state
    } else {
        PlayerState = new LivingClothState(Player.MemberNumbers)
    }
}

function stopCurse() {
    clearInterval(Interval)
    Interval = 0
    var content = "*" + AcquireplayerName(Player) + "身上的触手暂时安静下来了."
    SendChat("Emote", content)
}

function continueCurse() {
    if (!Interval) {
        Interval = setInterval(function(){ LivingClothProgress(Player) }, 10000)
        var content = "*" + AcquireplayerName(Player) + "身上的触手又活跃起来了."
        SendChat("Emote", content)
    }
}

Interval = 0
SatietyInterval = 0
RestoreInterval = 0
playerName = AcquireplayerName(Player)

function main() {
    init()
    if (!Interval) {
        Interval = setInterval(function(){ LivingClothProgress(Player) }, 10000)
    }
    if (!SatietyInterval) {
        SatietyInterval = setInterval(function(){ LivingClothSatiety(Player) }, 20000)
    }
    if (!RestoreInterval) {
        RestoreInterval = setInterval(function(){ LivingClothRestore(Player) }, 10000)
    }
    setInterval(function() { saveState() }, 10000)
}

main()
